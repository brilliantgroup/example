<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->unsigned();

            $table->foreign('category_id')
                ->references('id')->on('bikes_categories')
                ->onDelete('cascade');

            $table->string('name');
            $table->string('slug', 200)->nullable()->index();

            $table->bigInteger('brand_id')->unsigned();

            $table->foreign('brand_id')
                ->references('id')->on('bikes_brands')
                ->onDelete('cascade');

            $table->year('year')->nullable();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->text('features')->nullable();
            $table->text('components')->nullable();
            $table->boolean('newest')->default('0');
            $table->boolean('special_offer')->default('0');
            $table->boolean('sale')->default('0');
            $table->boolean('active')->default('1');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bikes');
    }
}
