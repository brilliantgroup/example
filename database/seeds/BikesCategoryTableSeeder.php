<?php

use Illuminate\Database\Seeder;
use App\Models\BikesCategory;

class BikesCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = ['Гібридні', 'Гірські', 'Міські', 'Дитячі', 'Шосейні'];

        foreach ($brands as $brand) {
            BikesCategory::create([
                'name' => $brand,
                'active' => '1'
            ]);
        }
    }
}
