<?php

use Illuminate\Database\Seeder;
use App\Models\ClothesCategory;

class ClothesCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = ['Бахіли', 'Веломайки', 'Велотруси', 'Велоштани','Куртки та дощовики','Повсякденний одяг','Шкарпетки', 'Шорти'];

        foreach ($brands as $brand) {
            ClothesCategory::create([
                'name' => $brand,
                'active' => '1'
            ]);
        }
    }
}
