<?php

use Illuminate\Database\Seeder;
use App\Models\SparesCategory;

class SparesCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = ['Рами'];

        foreach ($brands as $brand) {
            SparesCategory::create([
                'name' => $brand,
                'active' => '1'
            ]);
        }
    }
}
