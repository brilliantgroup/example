<?php

use Illuminate\Database\Seeder;
use App\Models\AccessoriesCategory;

class AccessoriesCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = ['Дзвоники', 'Сумки', 'Чохли', 'Світловідбиваючі елементи'];

        foreach ($brands as $brand) {
            AccessoriesCategory::create([
                'name' => $brand,
                'active' => '1'
            ]);
        }
    }
}
