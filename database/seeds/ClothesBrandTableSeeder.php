<?php

use Illuminate\Database\Seeder;
use App\Models\ClothesBrand;

class ClothesBrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = ['ONRIDE'];

        foreach ($brands as $brand) {
            ClothesBrand::create([
                'name' => $brand,
                'active' => '1'
            ]);
        }
    }
}
