<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call(AdminsTableSeeder::class);
         $this->call(BikesBrandTableSeeder::class);
         $this->call(BikesCategoryTableSeeder::class);
         $this->call(SparesBrandTableSeeder::class);
         $this->call(SparesCategoryTableSeeder::class);
         $this->call(AccessoriesBrandTableSeeder::class);
         $this->call(AccessoriesCategoryTableSeeder::class);
         $this->call(ClothesBrandTableSeeder::class);
         $this->call(ClothesCategoryTableSeeder::class);
    }
}
