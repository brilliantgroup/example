<?php

use Illuminate\Database\Seeder;
use App\Models\SparesBrand;

class SparesBrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = ['Pride', 'Cannondale', 'Stolen', 'Schwinn', 'Trek', 'HAIBIKE', 'GHOST', 'Merida'];

        foreach ($brands as $brand) {
            SparesBrand::create([
                'name' => $brand,
                'active' => '1'
            ]);
        }
    }
}
