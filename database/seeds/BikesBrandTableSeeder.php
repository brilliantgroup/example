<?php

use Illuminate\Database\Seeder;
use App\Models\BikesBrand;

class BikesBrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = ['Pride', 'Cannondale', 'Stolen', 'Schwinn', 'Trek', 'HAIBIKE', 'GHOST', 'Merida', 'Winner', 'Cyclone','Kinetic'];

        foreach ($brands as $brand) {
            BikesBrand::create([
                'name' => $brand,
                'active' => '1'
            ]);
        }
    }
}
