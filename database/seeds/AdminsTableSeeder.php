<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'        => 'Admin',
            'email'             => 'admin@test.com',
            'password'          => bcrypt('admin'),
            'role' => '1'
        ]);

        User::create([
            'name'        => 'Superadmin',
            'email'             => 'superadmin@test.com',
            'password'          => bcrypt('superadmin'),
            'role' => '1'
        ]);
    }
}
