<?php

namespace App\Exceptions;

use Exception;

class CustomException extends Exception
{
/**
 * @var string
 */
    private $userMessage;

    public function __construct(string $userMessage)
    {
        $this->userMessage = $userMessage;
        parent::__construct("Custom exception");
    }

    public function getUserMessage(): string
    {
        return $this->userMessage;
    }
}
