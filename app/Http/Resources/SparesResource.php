<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SparesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => str_pad($this->id, 7, "0", STR_PAD_LEFT),
            'name' => $this->name,
            'brand' => $this->brands->name,
            'type' => $this->categories->name,
            'year' => $this->year,
            'short_description' => $this->short_description,
            'description' => $this->description ?? false,
            'image' => $this->getFirstOrDefaultMediaUrl('images', 'full-size'),
            'features' => $this->transformFeatures($this->features),
            'components' => $this->transformComponents($this->components)
        ];
    }

    private function transformFeatures($features)
    {
        $result = array_filter(array_map('trim', explode('--', $features)));

        return !empty($result)? $result : false;
    }

    private function transformComponents($components)
    {
        $data = array_map('trim', explode('--', $components));

        foreach (array_filter($data) as $k => $row) {
            if (strpos($row, ':')) {
                list($key, $value) = array_map('trim', explode(':', $row));
                $result[] = [
                    "key" => $key,
                    "value" => $value
                ];
            }
        }

        return $result ?? false;
    }

}
