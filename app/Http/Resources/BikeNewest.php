<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BikeNewest extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'brand' => $this->brands->name,
            'brandslug' => $this->brands->slug,
            'type' => $this->categories->name,
            'name' => $this->name,
            'slug' => $this->slug,
            'year' => $this->year,
            'image' => $this->getFirstOrDefaultMediaUrl('images', 'thumb')
        ];
    }
}
