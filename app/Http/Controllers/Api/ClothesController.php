<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Clothes;
use Illuminate\Http\Request;
use App\Http\Resources\ClothesResource as ItemResource;
use App\Http\Resources\Clothes as ItemsResource;

class ClothesController extends Controller
{

    public function getItem(Request $request) {
        $item = Clothes::where('slug', $request->slug)->with(['brands', 'categories'])->first();

        if ($item) {
            return new ItemResource($item);
        } else {
            return response()->json(['message' => 'Not Found!'], 404);
        }
    }

    public function getItems(Request $request) {
        $category = ($request->category=='all')?false:$request->category;

        if ($category) {
            $items = Clothes::whereHas('categories', function ($query) use ($category) {
                    $query->where('slug', $category);
            })->with(['brands', 'categories'])->get();
        } else {
            $items = Clothes::with(['brands', 'categories'])->get();
        }

        return ItemsResource::collection($items);
    }

    public function getItemsSale(Request $request) {
        $category = ($request->category=='all')?false:$request->category;

        if ($category) {
            $items = Clothes::whereHas('clothes-sale', function ($query) use ($category) {
                $query->where('slug', $category);
            })->with(['brands', 'categories'])
              ->where('sale', 1)
              ->get();
        } else {
            $items = Clothes::with(['brands', 'categories'])
              ->where('sale', 1)
              ->get();
        }

        return ItemsResource::collection($items);
    }

}
