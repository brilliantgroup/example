<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AccessoriesCategory;
use App\Models\BikesCategory;
use App\Models\ClothesCategory;
use App\Models\SparesCategory;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public $menu = [
        "bikes" => [
            [
                "name" => 'Гибридные',
                'url' => 'aa'
            ],
            [
                "name" => 'Гибридные',
                'url' => 'bb'
            ],
            [
                "name" => 'Гибридные',
                'url' => 'cc'
            ],
            [
                "name" => 'Гибридные',
                'url' => 'dd'
            ],
        ],
        "spares" => [
            [
                "name" => 'dpares1',
                'url' => 'aa'
            ],
            [
                "name" => 'spareds2',
                'url' => 'bb'
            ],
            [
                "name" => 'spares3',
                'url' => 'cc'
            ],
        ],
    ];


    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $menu = [
            'bikes' => $this->getBikeCategories(),
            'accessories' => $this->getAccessoryCategories(),
            'spares' => $this->getSparesCategories(),
            'clothes' => $this->getClothesCategories()
        ];

        return response()->json($menu);
    }

    private function getBikeCategories() {
        $categories = BikesCategory::isActive()->get(['name', 'slug']);

        return $categories;
    }

    private function getSparesCategories() {
        $categories = SparesCategory::isActive()->get(['name', 'slug']);

        return $categories;
    }

    private function getAccessoryCategories() {
        $categories = AccessoriesCategory::isActive()->get(['name', 'slug']);

        return $categories;
    }

    private function getClothesCategories() {
        $categories = ClothesCategory::isActive()->get(['name', 'slug']);

        return $categories;
    }
}
