<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bike;
use Illuminate\Http\Request;
use App\Http\Resources\BikeResource as ItemResource;
use App\Http\Resources\BikeNewest as BikeNewestResource;
use App\Http\Resources\Bikes as ItemsResource;

class BikesController extends Controller
{
    private $bikes = [
      ['id'=>'1', 'brand'=>'Cannondale', 'image'=>'images/bike1-for-home.png', 'name'=>'CAAD1 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'2', 'brand'=>'Cannondale', 'image'=>'images/bike2-for-home.png', 'name'=>'CAAD2 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'3', 'brand'=>'Cannondale', 'image'=>'images/bike3-for-home.png', 'name'=>'CAAD3 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'4', 'brand'=>'Cannondale', 'image'=>'images/bike4-for-home.png', 'name'=>'CAAD4 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'5', 'brand'=>'Cannondale', 'image'=>'images/bike1-for-home.png', 'name'=>'CAAD1 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'6', 'brand'=>'Cannondale', 'image'=>'images/bike2-for-home.png', 'name'=>'CAAD2 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'7', 'brand'=>'Cannondale', 'image'=>'images/bike3-for-home.png', 'name'=>'CAAD3 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'8', 'brand'=>'Cannondale', 'image'=>'images/bike4-for-home.png', 'name'=>'CAAD4 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'5', 'brand'=>'Cannondale', 'image'=>'images/bike1-for-home.png', 'name'=>'CAAD1 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'6', 'brand'=>'Cannondale', 'image'=>'images/bike2-for-home.png', 'name'=>'CAAD2 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'7', 'brand'=>'Cannondale', 'image'=>'images/bike3-for-home.png', 'name'=>'CAAD3 TIAGRA', 'type'=>'Шосейний'],
      ['id'=>'8', 'brand'=>'Cannondale', 'image'=>'images/bike4-for-home.png', 'name'=>'CAAD4 TIAGRA', 'type'=>'Шосейний']
    ];



    public function index(Request $request) {
        $bikes = Bike::newestIndex()->with(['brands', 'categories'])->get();

        return BikeNewestResource::collection($bikes)->chunk(4);
    }

    public function getItem(Request $request) {
        $bike = Bike::where('slug', $request->slug)->with(['brands', 'categories'])->first();

        return new ItemResource($bike);
    }

    public function getItems(Request $request) {
        $category = ($request->category=='all')?false:$request->category;

        if ($category) {
            $bikes = Bike::whereHas('categories', function ($query) use ($category) {
                    $query->where('slug', $category);
            })
              ->where('active', 1)
              ->with(['brands', 'categories'])
              ->orderBy('year', 'desc')
              ->get();
        } else {
            $bikes = Bike::with(['brands', 'categories'])
              ->where('active', 1)
              ->orderBy('year', 'desc')
              ->get();
        }

//        return response()->json($bikes);

        return ItemsResource::collection($bikes);
    }

    public function getItemsSale(Request $request) {
        $category = ($request->category=='all')?false:$request->category;

        if ($category) {
            $bikes = Bike::whereHas('categories', function ($query) use ($category) {
                $query->where('slug', $category);
            })->with(['brands', 'categories'])
              ->where('sale', 1)
              ->get();
        } else {
            $bikes = Bike::with(['brands', 'categories'])->where('sale', 1)->get();
        }

        return ItemsResource::collection($bikes);
    }

}
