<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddClothes;
use App\Http\Requests\UpdateClothes;
use App\Models\Clothes;
use App\Models\ClothesBrand;
use App\Models\ClothesCategory;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;

class ClothesCatalogController extends Controller
{
    public function index()
    {
        $items = Clothes::with('categories')->get();

        return view('backend.clothes.catalog.index', [
                'items' => $items
            ]
        );
    }

    public function create(Request $request)
    {
        $categories = ClothesCategory::all();
        $brands = ClothesBrand::all();

        return view('backend.clothes.catalog.create', [
            'categories' => $categories,
            'brands' => $brands
        ]);
    }

    public function store(AddClothes $request)
    {
        $clothes = new Clothes($request->except('image'));
        $clothes->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $clothes->addMediaFromRequest('image')->toMediaCollection('images');
        }

        return redirect()->route('clothes.catalog.index')->with('success', __('forms.message.success'));
    }

    public function edit(Clothes $item)
    {
        $categories = ClothesCategory::all();
        $brands = ClothesBrand::all();

        return view('backend.clothes.catalog.edit', [
            'item' => $item,
            'categories' => $categories,
            'brands' => $brands
        ]);
    }

    public function update(UpdateClothes $request, Clothes $item)
    {
        $request->request->add(['slug' => '']);
        $request->request->add(['newest' => $request->input('newest', false)]);
        $request->request->add(['special_offer' => $request->input('special_offer', false)]);
        $request->request->add(['sale' => $request->input('sale', false)]);
        $request->request->add(['active' => $request->input('active', false)]);

        $clothes = Clothes::find($item->id);
        $clothes->update($request->except('image'));

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $clothes->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->route('clothes.catalog.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $item = Clothes::find($id);

        if (isset($item->media()->first()->id)) {
            $item->deleteMedia($item->media()->first()->id);
        }

        $item->delete();
    }
}
