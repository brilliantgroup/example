<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddClothesBrand;
use App\Http\Requests\UpdateClothesBrand;
use App\Models\ClothesBrand;
use Illuminate\Http\Request;

class ClothesBrandController extends Controller
{
    public function index()
    {
        $brands = ClothesBrand::all();

        return view("backend.clothes.brands.index", ["brands" => $brands]);
    }

    public function create(Request $request)
    {
        return view("backend.clothes.brands.create");
    }

    public function store(AddClothesBrand $request)
    {
        ClothesBrand::create($request->all());

        return redirect()->route('clothes.brand.index')->with('success', __('forms.message.success'));
    }

    public function edit(ClothesBrand $brand)
    {
        return view("backend.clothes.brands.edit", ['brand' => $brand]);
    }

    public function update(UpdateClothesBrand $request, ClothesBrand $brand)
    {
        $request->request->add(['slug' => '']);

        ClothesBrand::find($brand->id)->update($request->only('name', 'active', 'slug'));

        return redirect()->route('clothes.brand.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $brand = ClothesBrand::find($id);
        $brand->delete();
    }
}
