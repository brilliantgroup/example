<?php

namespace App\Http\Controllers\Backend;

use App\Models\Accessory;
use App\Models\Clothes;
use App\Models\Spares;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bike;

class DashboardController extends Controller
{
    public function index()
    {

/*
        $bikes = Bike::with('brands')->get();
        $acc = Accessory::all();
        $clothes = Clothes::all();
        $spares = Spares::all();

        $_out = '<?xml version="1.0" encoding="UTF-8"?>
                <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';


        $_array = ['', 'bikes', 'spares', 'accessories', 'clothes', 'where-to-buy', 'warranty', 'about'];

        foreach ($_array as $a) {
            $_out .= '<url>';
            $_out .= '<loc>http://roverbike.com.ua/' . $a . '</loc>';
            $_out .= '</url>';
        }

        foreach ($bikes as $bike) {
            $_out .= '<url>';
            $_out .= '<loc>http://roverbike.com.ua/bike/' . $bike->brands->slug . '/' . $bike->slug . '</loc>';
            $_out .= '</url>';
        }

        foreach ($acc as $ac) {
            $_out .= '<url>';
            $_out .= '<loc>http://roverbike.com.ua/accessories/' . $ac->brands->slug . '/' . $ac->slug . '</loc>';
            $_out .= '</url>';
        }

        foreach ($clothes as $cl) {
            $_out .= '<url>';
            $_out .= '<loc>http://roverbike.com.ua/clothes/' . $cl->brands->slug . '/' . $cl->slug . '</loc>';
            $_out .= '</url>';
        }

        foreach ($spares as $sp) {
            $_out .= '<url>';
            $_out .= '<loc>http://roverbike.com.ua/spares/' . $sp->brands->slug . '/' . $sp->slug . '</loc>';
            $_out .= '</url>';
        }


        $_out .= '</urlset>';

        echo $_out;
*/


        return view("backend.dashboard");
    }
}
