<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddAccesoriesCategory;
use App\Http\Requests\UpdateAccesoriesCategory;
use App\Models\AccessoriesCategory;
use Illuminate\Http\Request;

class AccessoriesCategoryController extends Controller
{
    public function index()
    {
        $categories = AccessoriesCategory::all();

        return view("backend.accessories.category.index", ["categories" => $categories]);
    }

    public function create()
    {
        return view("backend.accessories.category.create");
    }

    public function store(AddAccesoriesCategory $request)
    {
        AccessoriesCategory::create($request->all());

        return redirect()->route('accessories.category.index')->with('success', __('forms.message.success'));
    }

    public function edit(AccessoriesCategory $category)
    {
        return view("backend.accessories.category.edit", ['category' => $category]);
    }

    public function update(UpdateAccesoriesCategory $request, AccessoriesCategory $category)
    {
        $request->request->add(['slug' => '']);

        AccessoriesCategory::find($category->id)->update($request->only('name', 'active', 'slug'));

        return redirect()->route('accessories.category.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $category = AccessoriesCategory::find($id);
        $category->delete();
    }
}
