<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddBikes;
use App\Http\Requests\UpdateBikes;
use App\Models\Bike;
use App\Models\BikesBrand;
use App\Models\BikesCategory;
use App\Services\BikeCatalogService;
use Illuminate\Http\Request;

class BikesCatalogController extends Controller
{
    public function index()
    {
        $items = Bike::with('categories')->get();

        return view('backend.bikes.catalog.index', [
                'items' => $items
            ]
        );
    }

    public function create(Request $request)
    {
        $categories = BikesCategory::all();
        $brands = BikesBrand::all();

        return view('backend.bikes.catalog.create', [
            'categories' => $categories,
            'brands' => $brands
        ]);
    }

    public function store(AddBikes $request, BikeCatalogService $bikeCatalogService)
    {
        $bikeCatalogService->create($request->getDto());

        return redirect()->route('bikes.catalog.index')->with('success', __('forms.message.success'));
    }

    public function edit(Bike $item)
    {
        $categories = BikesCategory::all();
        $brands = BikesBrand::all();

        return view('backend.bikes.catalog.edit', [
            'item' => $item,
            'categories' => $categories,
            'brands' => $brands
        ]);
    }

    public function update(UpdateBikes $request, Bike $item)
    {
        $request->request->add(['slug' => '']);
        $request->request->add(['newest' => $request->input('newest', false)]);
        $request->request->add(['special_offer' => $request->input('special_offer', false)]);
        $request->request->add(['sale' => $request->input('sale', false)]);
        $request->request->add(['active' => $request->input('active', false)]);

        $bike = Bike::find($item->id);
        $bike->update($request->except('image'));

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $bike->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->route('bikes.catalog.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $item = Bike::find($id);

        if (isset($item->media()->first()->id)) {
            $item->deleteMedia($item->media()->first()->id);
        }

        $item->delete();
    }
}
