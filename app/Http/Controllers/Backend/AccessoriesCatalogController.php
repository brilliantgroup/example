<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddAccessories;
use App\Http\Requests\UpdateAccessories;
use App\Models\AccessoriesCategory;
use App\Models\AccessoriesBrand;
use App\Models\Accessory;
use Illuminate\Http\Request;

class AccessoriesCatalogController extends Controller
{
    public function index() {
        $items = Accessory::with('categories')->get();

        return view('backend.accessories.catalog.index', [
                'items' => $items
            ]
        );
    }

    public function create(Request $request)
    {
        $categories = AccessoriesCategory::all();
        $brands = AccessoriesBrand::all();

        return view('backend.accessories.catalog.create', [
            'categories' => $categories,
            'brands' => $brands
        ]);
    }

    public function store(AddAccessories $request)
    {
        $accessories = new Accessory($request->except('image'));
        $accessories->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $accessories->addMediaFromRequest('image')->toMediaCollection('images');
        }

        return redirect()->route('accessories.catalog.index')->with('success', __('forms.message.success'));
    }

    public function edit(Accessory $item)
    {
        $categories = AccessoriesCategory::all();
        $brands = AccessoriesBrand::all();

        return view('backend.accessories.catalog.edit', [
            'item' => $item,
            'categories' => $categories,
            'brands' => $brands
        ]);
    }

    public function update(UpdateAccessories $request, Accessory $item)
    {
        $request->request->add(['slug' => '']);
        $request->request->add(['newest' => $request->input('newest', false)]);
        $request->request->add(['special_offer' => $request->input('special_offer', false)]);
        $request->request->add(['sale' => $request->input('sale', false)]);
        $request->request->add(['active' => $request->input('active', false)]);

        $accessories = Accessory::find($item->id);
        $accessories->update($request->except('image'));

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $accessories->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->route('accessories.catalog.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $item = Accessory::find($id);

        if (isset($item->media()->first()->id)) {
            $item->deleteMedia($item->media()->first()->id);
        }

        $item->delete();
    }
}
