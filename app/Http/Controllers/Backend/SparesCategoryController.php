<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddSparesCategory;
use App\Http\Requests\UpdateSparesCategory;
use App\Models\SparesCategory;

class SparesCategoryController extends Controller
{
    public function index()
    {
        $categories = SparesCategory::all();

        return view("backend.spares.category.index", ["categories" => $categories]);
    }

    public function create()
    {
        return view("backend.spares.category.create");
    }

    public function store(AddSparesCategory $request)
    {
        SparesCategory::create($request->all());

        return redirect()->route('spares.category.index')->with('success', __('forms.message.success'));
    }

    public function edit(SparesCategory $category)
    {
        return view("backend.spares.category.edit", ['category' => $category]);
    }

    public function update(UpdateSparesCategory $request, SparesCategory $category)
    {
        $request->request->add(['slug' => '']);

        SparesCategory::find($category->id)->update($request->only('name', 'active', 'slug'));

        return redirect()->route('spares.category.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $category = SparesCategory::find($id);
        $category->delete();
    }
}
