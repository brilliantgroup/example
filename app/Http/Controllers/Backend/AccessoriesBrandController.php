<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddAccesoriesBrand;
use App\Http\Requests\UpdateAccesoriesBrand;
use App\Models\AccessoriesBrand;
use Illuminate\Http\Request;

class AccessoriesBrandController extends Controller
{
    public function index()
    {
        $brands = AccessoriesBrand::all();

        return view("backend.accessories.brands.index", ["brands" => $brands]);
    }

    public function create(Request $request)
    {
        return view("backend.accessories.brands.create");
    }

    public function store(AddAccesoriesBrand $request)
    {
        AccessoriesBrand::create($request->all());

        return redirect()->route('accessories.brand.index')->with('success', __('forms.message.success'));
    }

    public function edit(AccessoriesBrand $brand)
    {
        return view("backend.accessories.brands.edit", ['brand' => $brand]);
    }

    public function update(UpdateAccesoriesBrand $request, AccessoriesBrand $brand)
    {
        $request->request->add(['slug' => '']);

        AccessoriesBrand::find($brand->id)->update($request->only('name', 'active', 'slug'));

        return redirect()->route('accessories.brand.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $brand = AccessoriesBrand::find($id);
        $brand->delete();
    }
}
