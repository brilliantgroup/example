<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddClothesCategory;
use App\Http\Requests\UpdateClothesCategory;
use App\Models\ClothesCategory;
use Illuminate\Http\Request;

class ClothesCategoryController extends Controller
{
    public function index()
    {
        $categories = ClothesCategory::all();

        return view("backend.clothes.category.index", ["categories" => $categories]);
    }

    public function create()
    {
        return view("backend.clothes.category.create");
    }

    public function store(AddClothesCategory $request)
    {
        ClothesCategory::create($request->all());

        return redirect()->route('clothes.category.index')->with('success', __('forms.message.success'));
    }

    public function edit(ClothesCategory $category)
    {
        return view("backend.clothes.category.edit", ['category' => $category]);
    }

    public function update(UpdateClothesCategory $request, ClothesCategory $category)
    {
        $request->request->add(['slug' => '']);

        ClothesCategory::find($category->id)->update($request->only('name', 'active', 'slug'));

        return redirect()->route('clothes.category.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $category = ClothesCategory::find($id);
        $category->delete();
    }
}
