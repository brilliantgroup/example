<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddSparesBrand;
use App\Http\Requests\UpdateSparesBrand;
use App\Models\SparesBrand;
use Illuminate\Http\Request;

class SparesBrandController extends Controller
{
    public function index()
    {
        $brands = SparesBrand::all();

        return view("backend.spares.brands.index", ["brands" => $brands]);
    }

    public function create(Request $request)
    {
        return view("backend.spares.brands.create");
    }

    public function store(AddSparesBrand $request)
    {
        SparesBrand::create($request->all());

        return redirect()->route('spares.brand.index')->with('success', __('forms.message.success'));
    }

    public function edit(SparesBrand $brand)
    {
        return view("backend.spares.brands.edit", ['brand' => $brand]);
    }

    public function update(UpdateSparesBrand $request, SparesBrand $brand)
    {
        $request->request->add(['slug' => '']);

        SparesBrand::find($brand->id)->update($request->only('name', 'active', 'slug'));

        return redirect()->route('spares.brand.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $brand = SparesBrand::find($id);
        $brand->delete();
    }
}
