<?php

namespace App\Http\Controllers\Backend;

use App\Models\BikesBrand;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddBikesBrand;
use App\Http\Requests\UpdateBikesBrand;
use App\Services\BikeBrandService;
use Illuminate\Http\Request;

class BikesBrandController extends Controller
{
    public function index(BikeBrandService $bikeBrandService)
    {
        $brands = $bikeBrandService->fetch();

        return view("backend.bikes.brands.index", ["brands" => $brands]);
    }

    public function create(Request $request)
    {
        return view("backend.bikes.brands.create");
    }

    public function store(AddBikesBrand $request, BikeBrandService $bikeBrandService)
    {
        $bikeBrandService->create($request->getDto());

        return redirect()->route('bikes.brand.index')->with('success', __('forms.message.success'));
    }

    public function edit(BikesBrand $brand)
    {
        return view("backend.bikes.brands.edit", ['brand' => $brand]);
    }

    public function update(UpdateBikesBrand $request,
                           BikeBrandService $bikeBrandService,
                           BikesBrand $brand)
    {

        $bikeBrandService->update($request->getDto(), $brand->id);
        return redirect()->route('bikes.brand.index')->with('success', __('forms.message.success'));
    }

    public function destroy(BikeBrandService $bikeBrandService, $id)
    {
        $bikeBrandService->delete($id);
    }

}
