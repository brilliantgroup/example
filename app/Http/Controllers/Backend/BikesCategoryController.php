<?php

namespace App\Http\Controllers\Backend;

use App\Models\BikesCategory;
use App\Http\Requests\AddBikesCategoryRequest;
use App\Http\Requests\UpdateBikesCategoryRequest;

use App\Http\Controllers\Controller;
use App\Services\BikeCategoryService;

class BikesCategoryController extends Controller
{

    /**
     * @var BikesCategory
     */
    private $category;

    /**
     * BikesCategoryController constructor.
     */
    public function __construct(BikesCategory $bikesCategory)
    {
        $this->category = $bikesCategory;
    }

    /**
     * @param BikeCategoryService $bikeCategoryService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(BikeCategoryService $bikeCategoryService)
    {
        $categories = $bikeCategoryService->fetch();

        return view("backend.bikes.category.index", ["categories" => $categories]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view("backend.bikes.category.create");
    }

    /**
     * @param AddBikesCategoryRequest $request
     * @param BikeCategoryService $bikeCategoryService
     * @return false|\Illuminate\Http\RedirectResponse
     */
    public function store(AddBikesCategoryRequest $request, BikeCategoryService $bikeCategoryService)
    {
        if (!$bikeCategoryService->create($request->getDto())) {
            return false;
        }

        return redirect()->route('bikes.category.index')->with('success', __('forms.message.success'));
    }

    /**
     * @param BikesCategory $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(BikesCategory $category)
    {
        return view("backend.bikes.category.edit", ['category' => $category]);
    }

    /**
     * @param UpdateBikesCategoryRequest $request
     * @param BikeCategoryService $bikeCategoryService
     * @param BikesCategory $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateBikesCategoryRequest $request,
                           BikeCategoryService $bikeCategoryService,
                           BikesCategory $category)
    {

        if (!$bikeCategoryService->update($request->getDto(), $category)) {
            return redirect()->route('bikes.category.index')->with('success', __('forms.message.error'));
        }

        return redirect()->route('bikes.category.index')->with('success', __('forms.message.success'));
    }

    /**
     * @param $id
     * @param BikeCategoryService $bikeCategoryService
     */
    public function destroy($id, BikeCategoryService $bikeCategoryService)
    {
        $bikeCategoryService->delete($id);
    }
}
