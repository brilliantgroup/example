<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddSpares;
use App\Http\Requests\UpdateSpares;
use App\Models\Spares;
use App\Models\SparesBrand;
use App\Models\SparesCategory;
use Illuminate\Http\Request;

class SparesCatalogController extends Controller
{
    public function index()
    {
        $items = Spares::with('categories')->get();

        return view('backend.spares.catalog.index', [
                'items' => $items
            ]
        );
    }

    public function create(Request $request)
    {
        $categories = SparesCategory::all();
        $brands = SparesBrand::all();

        return view('backend.spares.catalog.create', [
            'categories' => $categories,
            'brands' => $brands
        ]);
    }

    public function store(AddSpares $request)
    {
        $spares = new Spares($request->except('image'));
        $spares->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $spares->addMediaFromRequest('image')->toMediaCollection('images');
        }

        return redirect()->route('spares.catalog.index')->with('success', __('forms.message.success'));
    }

    public function edit(Spares $item)
    {
        $categories = SparesCategory::all();
        $brands = SparesBrand::all();

        return view('backend.spares.catalog.edit', [
            'item' => $item,
            'categories' => $categories,
            'brands' => $brands
        ]);
    }

    public function update(UpdateSpares $request, Spares $item)
    {
        $request->request->add(['slug' => '']);
        $request->request->add(['newest' => $request->input('newest', false)]);
        $request->request->add(['special_offer' => $request->input('special_offer', false)]);
        $request->request->add(['sale' => $request->input('sale', false)]);
        $request->request->add(['active' => $request->input('active', false)]);

        $spares = Spares::find($item->id);
        $spares->update($request->except('image'));

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $spares->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->route('spares.catalog.index')->with('success', __('forms.message.success'));
    }

    public function destroy($id)
    {
        $item = Spares::find($id);

        if (isset($item->media()->first()->id)) {
            $item->deleteMedia($item->media()->first()->id);
        }

        $item->delete();
    }
}
