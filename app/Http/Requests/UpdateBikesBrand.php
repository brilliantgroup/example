<?php

namespace App\Http\Requests;

use App\Dto\BikeBrandUpdateDto;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBikesBrand extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Поле обовязкове для заповнення',
            'name.min' => 'Назва повинна бути не коротше :min символів'
        ];
    }

    public function getDto(): BikeBrandUpdateDto
    {
        return new BikeBrandUpdateDto(
          $this->get('name'),
          $this->get('usd'),
          $this->has('active')
        );
    }
}
