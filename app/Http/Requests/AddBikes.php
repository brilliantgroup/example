<?php

namespace App\Http\Requests;

use App\Dto\BikeCatalogCreateDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;

class AddBikes extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|min:3'
        ];
    }

    public function messages()
    {
        return [
          'name.required' => 'Поле обовязкове для заповнення',
          'name.min' => 'Назва повинна бути не коротше :min символів'
        ];
    }

    public function getDto(): BikeCatalogCreateDto
    {
        return new BikeCatalogCreateDto(
          $this->get('name'),
          $this->get('category_id'),
          $this->get('brand_id'),
          $this->get('year'),
          $this->get('short_description') ?? '',
          $this->get('description') ?? '',
          $this->get('features') ?? '',
          $this->get('components') ?? '',
          $this->has('newest') ?? false,
          $this->has('special_offer') ?? false,
          $this->has('sale') ?? false,
          $this->get('price') ?? '0.00',
          $this->has('active') ?? false,
          $this->file('image') ?? ''
        );
    }
}
