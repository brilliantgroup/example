<?php


namespace App\Dto;

/**
 * Class BikeCategoryCreateDto
 * @package App\Dto
 */
class BikeCategoryCreateDto
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var bool
     */
    private $isActive;

    /**
     * BikeCategoryCreateDto constructor.
     * @param string $name
     * @param bool $isActive
     */
    public function __construct(string $name, bool $isActive)
    {
        $this->name = $name;
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }





}