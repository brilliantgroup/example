<?php
namespace App\Dto;


class BikeBrandCreateDto
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var float
     */
    private $usd;

    /**
     * @var bool
     */
    private $isActive;

    /**
     * BikeBrandCreateDto constructor.
     * @param string $name
     * @param string $slug
     * @param float $usd
     * @param bool $isActive
     */
    public function __construct(string $name, float $usd, bool $isActive)
    {
        $this->name = $name;
        $this->usd = $usd;
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return float
     */
    public function getUsd(): float
    {
        return $this->usd;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

}