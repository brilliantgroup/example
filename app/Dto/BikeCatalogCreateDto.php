<?php


namespace App\Dto;


use Illuminate\Http\UploadedFile;

class BikeCatalogCreateDto
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $category_id;

    /**
     * @var int
     */
    private $brand_id;

    /**
     * @var int
     */
    private $year;

    /**
     * @var string
     */
    private $short_description;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $features;

    /**
     * @var string
     */
    private $components;

    /**
     * @var bool
     */
    private $newest;

    /**
     * @var bool
     */
    private $special_offer;

    /**
     * @var bool
     */
    private $sale;

    /**
     * @var float
     */
    private $price;

    /**
     * @var bool
     */
    private $active;
    private $image;

    /**
     * BikeCatalogCreateDto constructor.
     * @param string $name
     * @param int $category_id
     * @param int $brand_id
     * @param int $year
     * @param string $short_description
     * @param string $description
     * @param string $features
     * @param string $components
     * @param bool $newest
     * @param bool $special_offer
     * @param bool $sale
     * @param float $price
     * @param bool $active
     * @param $image
     */
    public function __construct(string $name, int $category_id, int $brand_id, int $year, string $short_description, string $description, string $features, string $components, bool $newest, bool $special_offer, bool $sale, float $price, bool $active, $image)
    {
        $this->name = $name;
        $this->category_id = $category_id;
        $this->brand_id = $brand_id;
        $this->year = $year;
        $this->short_description = $short_description;
        $this->description = $description;
        $this->features = $features;
        $this->components = $components;
        $this->newest = $newest;
        $this->special_offer = $special_offer;
        $this->sale = $sale;
        $this->price = $price;
        $this->active = $active;
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @return int
     */
    public function getBrandId(): int
    {
        return $this->brand_id;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->short_description;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getFeatures(): string
    {
        return $this->features;
    }

    /**
     * @return string
     */
    public function getComponents(): string
    {
        return $this->components;
    }

    /**
     * @return bool
     */
    public function isNewest(): bool
    {
        return $this->newest;
    }

    /**
     * @return bool
     */
    public function isSpecialOffer(): bool
    {
        return $this->special_offer;
    }

    /**
     * @return bool
     */
    public function isSale(): bool
    {
        return $this->sale;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return
     */
    public function getImage()
    {
        return $this->image;
    }



}