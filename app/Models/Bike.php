<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BikesBrand;
use App\Models\BikesCategory;

//Media Library
use Spatie\MediaLibrary\HasMedia\HasMedia;
Use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use Spatie\MediaLibrary\Models\Media;


class Bike extends Model implements HasMedia
{
    use SoftDeletes;
    use Sluggable;

    use HasMediaTrait;

    protected $fillable = [
        'name',
        'category_id',
        'brand_id',
        'year',
        'short_description',
        'description',
        'features',
        'components',
        'newest',
        'special_offer',
        'sale',
        'active'
    ];

    protected $casts = [
        'newest' => 'boolean',
        'special_offer' => 'boolean',
        'sale' => 'boolean',
        'price' => 'float',
        'active' => 'boolean',
    ];

    protected $attributes = [
        'newest' => false,
        'special_offer' => false,
        'sale' => false,
        'active' => true
    ];

    protected $defaultImage = '/backend/images/default/index.png';

    public function categories() {
        return $this->hasOne(BikesCategory::class, 'id', 'category_id');
    }

    public function brands() {
        return $this->hasOne(BikesBrand::class, 'id', 'brand_id');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('admin-thumb')
            ->width(50)
            ->height(50);

        $this->addMediaConversion('thumb')
            ->width(500)
            ->height(500);


        $this->addMediaConversion('full-size');
    }

    public function getFirstOrDefaultMediaUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
        $url = $this->getFirstMediaUrl($collectionName, $conversionName);

        return $url ? $url : $this->defaultImage ?? '';
    }

    public function scopeNewestIndex($query){
        return $query->where('newest', 1);
    }
}
