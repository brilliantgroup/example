<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;


//Media Library
use Spatie\MediaLibrary\HasMedia\HasMedia;
Use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use Spatie\MediaLibrary\Models\Media;


class Clothes extends Model implements HasMedia
{
    use SoftDeletes;
    use Sluggable;

    use HasMediaTrait;

    protected $fillable = [
        'name',
        'category_id',
        'brand_id',
        'short_description',
        'description',
        'features',
        'components',
        'newest',
        'special_offer',
        'sale',
        'active'
    ];

    protected $casts = [
        'newest' => 'boolean',
        'special_offer' => 'boolean',
        'sale' => 'boolean',
        'active' => 'boolean',
    ];

    protected $attributes = [
        'newest' => false,
        'special_offer' => false,
        'sale' => false,
        'active' => true
    ];

    protected $defaultImage = '/backend/images/default/index.png';

    public function categories() {
        return $this->hasOne(ClothesCategory::class, 'id', 'category_id');
    }

    public function brands() {
        return $this->hasOne(ClothesBrand::class, 'id', 'brand_id');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('admin-thumb')
            ->width(50)
            ->height(50);

        $this->addMediaConversion('thumb')
            ->width(150)
            ->height(150);


        $this->addMediaConversion('full-size')
            ->withResponsiveImages();
    }

    public function getFirstOrDefaultMediaUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
        $url = $this->getFirstMediaUrl($collectionName, $conversionName);

        return $url ? $url : $this->defaultImage ?? '';
    }
}
