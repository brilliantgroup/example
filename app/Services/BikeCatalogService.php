<?php


namespace App\Services;

use App\Dto\BikeCatalogCreateDto;
use App\Dto\BikeCatalogUpdateDto;
use App\Exceptions\CustomException;
use App\Models\Bike;
use App\Models\BikesBrand;
use App\Models\BikesCategory;

final class BikeCatalogService
{
    private $bike;
    private $bikesCategory;
    private $bikesBrand;

    public function __construct(Bike $bike,
                                BikesCategory $bikesCategory,
                                BikesBrand $bikesBrand)
    {
        $this->bike = $bike;
        $this->bikesCategory = $bikesCategory;
        $this->bikesBrand = $bikesBrand;
    }

    public function fetch()
    {
        return $this->bike->all();
    }

    public function create(BikeCatalogCreateDto $bikeCatalogCreateDto)
    {
        $category = $this->bikesCategory->find($bikeCatalogCreateDto->getCategoryId());

        if (!$category) {
            throw new CustomException('Incorrect Category ID');
        }

        $brand = $this->bikesBrand->find($bikeCatalogCreateDto->getBrandId());

        if (!$brand) {
            throw new CustomException('Incorrect Brand ID');
        }

        $this->bike->name = $bikeCatalogCreateDto->getName();
        $this->bike->category_id = $bikeCatalogCreateDto->getCategoryId();
        $this->bike->brand_id = $bikeCatalogCreateDto->getBrandId();
        $this->bike->year = $bikeCatalogCreateDto->getYear();
        $this->bike->short_description = $bikeCatalogCreateDto->getShortDescription();
        $this->bike->description = $bikeCatalogCreateDto->getDescription();
        $this->bike->features = $bikeCatalogCreateDto->getFeatures();
        $this->bike->components = $bikeCatalogCreateDto->getComponents();
        $this->bike->newest = $bikeCatalogCreateDto->isNewest();
        $this->bike->special_offer = $bikeCatalogCreateDto->isSpecialOffer();
        $this->bike->sale = $bikeCatalogCreateDto->isSale();
        $this->bike->price = $bikeCatalogCreateDto->getPrice();
        $this->bike->active = $bikeCatalogCreateDto->isActive();

        $this->bike->save();

        if ($bikeCatalogCreateDto->getImage()) {
            $this->bike->addMediaFromRequest('image')->toMediaCollection('images');
        }

        return true;
    }

    public function update(BikeCatalogUpdateDto $bikeCatalogUpdateDto,
                           int $id)
    {
        $bike = $this->bike->find($id);

        if (!$bike) {
            throw new CustomException('Incorrect resource ID');
        }

        $category = $this->bikesCategory->find($bikeCatalogUpdateDto->getCategoryId());

        if (!$category) {
            throw new CustomException('Incorrect Category ID');
        }

        $brand = $this->bikesBrand->find($bikeCatalogUpdateDto->getBrandId());

        if (!$$brand) {
            throw new CustomException('Incorrect Brand ID');
        }

        $this->bike->name = $bikeCatalogUpdateDto->getName();
        $this->bike->category_id = $bikeCatalogUpdateDto->getCategoryId();
        $this->bike->brand_id = $bikeCatalogUpdateDto->getBrandId();
        $this->bike->year = $bikeCatalogUpdateDto->getYear();
        $this->bike->short_description = $bikeCatalogUpdateDto->getShortDescription();
        $this->bike->description = $bikeCatalogUpdateDto->getDescription();
        $this->bike->features = $bikeCatalogUpdateDto->getFeatures();
        $this->bike->components = $bikeCatalogUpdateDto->getComponents();
        $this->bike->newest = $bikeCatalogUpdateDto->isNewest();
        $this->bike->special_offer = $bikeCatalogUpdateDto->isSpecialOffer();
        $this->bike->sale = $bikeCatalogUpdateDto->isSale();
        $this->bike->price = $bikeCatalogUpdateDto->getPrice();
        $this->bike->active = $bikeCatalogUpdateDto->isActive();

        $this->bike->save();

        if ($bikeCatalogUpdateDto->getImage()) {
            $this->bike->addMediaFromRequest('image')->toMediaCollection('images');
        }

        return true;
    }

    public function delete($id)
    {
        $brand = $this->bike->find($id);

        if (!$brand) {
            throw new CustomException('Incorrect Source ID');
        }

        $brand->delete();
    }

}