<?php

namespace App\Services;


use App\Dto\BikeCategoryCreateDto;
use App\Dto\BikeCategoryUpdateDto;
use App\Exceptions\CustomException;
use App\Models\BikesCategory;

/**
 * Class BikeCategoryService
 * @package App\Services
 */
final class BikeCategoryService
{
    private $bikesCategory;

    public function __construct(BikesCategory $bikesCategory) {
      $this->bikesCategory = $bikesCategory;
    }

    /**
     * @return BikesCategory[]|\Illuminate\Database\Eloquent\Collection
     */
    public function fetch(){
        return $this->bikesCategory->all();
    }

    /**
     * @param BikeCategoryCreateDto $request
     * @return bool
     */
    public function create(BikeCategoryCreateDto $request)
    {
        $this->bikesCategory->name = $request->getName();
        $this->bikesCategory->active = $request->isActive();
        $this->bikesCategory->save();

        return true;
    }

    /**
     * @param BikeCategoryUpdateDto $request
     * @param BikesCategory $bikesCategory
     * @return bool
     *
     */

    public function update(BikeCategoryUpdateDto $request, BikesCategory $category)
    {
        $category = $this->bikesCategory->find($category->id);

        if (!$category) {
            throw new CustomException('Incorrect Source ID');
        }

        $category->name = $request->getName();
        $category->active = $request->isActive();
        $category->save();

        return true;
    }

    /**
     * @param $id
     */
    public function delete($id) {
        $category = $this->bikesCategory->find($id);

        if (!$category) {
            throw new CustomException('Incorrect Source ID');
        }

        $category->delete();
    }

}