<?php


namespace App\Services;

use App\Dto\BikeBrandCreateDto;
use App\Dto\BikeBrandUpdateDto;
use App\Exceptions\CustomException;
use App\Models\BikesBrand;

final class BikeBrandService
{
    private $bikesBrand;

    public function __construct(BikesBrand $bikesBrand)
    {
        $this->bikesBrand = $bikesBrand;
    }

    public function fetch()
    {
        return $this->bikesBrand->all();
    }

    public function create(BikeBrandCreateDto $bikeBrandCreateDto)
    {

        $this->bikesBrand->name = $bikeBrandCreateDto->getName();
        $this->bikesBrand->usd = $bikeBrandCreateDto->getUsd();
        $this->bikesBrand->active = $bikeBrandCreateDto->isActive();
        $this->bikesBrand->save();

        return true;
    }

    public function update(BikeBrandUpdateDto $bikeBrandUpdateDto, int $id)
    {
        $bikesBrand = $this->bikesBrand->find($id);

        if (!$bikesBrand) {
            throw new CustomException('Incorrect Source ID');
        }

        $bikesBrand->name = $bikeBrandUpdateDto->getName();
        $bikesBrand->usd = $bikeBrandUpdateDto->getUsd();
        $bikesBrand->active = $bikeBrandUpdateDto->isActive();

        $bikesBrand->save();

        return true;
    }

    public function delete($id)
    {
        $brand = $this->bikesBrand->find($id);

        if (!$brand) {
            throw new CustomException('Incorrect Source ID');
        }

        $brand->delete();
    }

}