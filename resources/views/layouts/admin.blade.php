<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{ asset('backend/css/style.default.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/jquery.datatables.css') }}" rel="stylesheet">

</head>
<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

    <div class="leftpanel">

        <div class="logopanel">
            <h1><span>[</span> Ровербайк <span>]</span></h1>
        </div><!-- logopanel -->

        <div class="leftpanelinner">
            <!-- This is only visible to small devices -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media userlogged">
                    <img alt="" src="{{ asset('/backend/images/photos/loggeduser.png') }}" class="media-object">
                    <div class="media-body">
                        <h4>{{ Auth::user()->name }}</h4>
                    </div>
                </div>

                <h5 class="sidebartitle actitle">Акаунт</h5>
                <ul class="nav nav-pills nav-stacked nav-bracket mb30">
                    <li><a href="profile.html"><i class="fa fa-user"></i> <span>Профіль</span></a></li>
                    <li><a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                class="fa fa-sign-out"></i> Вихід</a></li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
@php
  $currentRoute = Route::currentRouteName();
@endphp
            <h5 class="sidebartitle">Меню</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket">
                <li class="{{ strstr($currentRoute, 'dashboard')?'active':'' }}"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> <span>Головна</span></a></li>
                <li class="nav-parent"><a href=""><i class="glyphicon glyphicon-star"></i> <span>Велосипеди</span></a>
                    <ul class="children">
                        <li class="{{ strstr($currentRoute,'bikes.catalog')?'active':'' }}"><a href="{{ route('bikes.catalog.index') }}"><i class="fa fa-book"></i> Каталог</a></li>
                        <li class="{{ strstr($currentRoute,'bikes.brand')?'active':'' }}"><a href="{{ route('bikes.brand.index') }}"><i class="fa fa-heart"></i> Бренди</a></li>
                        <li class="{{ strstr($currentRoute,'bikes.category')?'active':'' }}"><a href="{{ route('bikes.category.index') }}"><i class="fa fa-bars"></i> Категорії</a></li>
                    </ul>
                </li>
                <li class="nav-parent"><a href=""><i class="fa fa-cogs"></i> <span>Запчастини</span></a>
                    <ul class="children">
                        <li class="{{ strstr($currentRoute,'spares.catalog')?'active':'' }}"><a href="{{ route('spares.catalog.index') }}"><i class="fa fa-book"></i> Каталог</a></li>
                        <li class="{{ strstr($currentRoute,'spares.brand')?'active':'' }}"><a href="{{ route('spares.brand.index') }}"><i class="fa fa-heart"></i> Бренди</a></li>
                        <li class="{{ strstr($currentRoute,'spares.category')?'active':'' }}"><a href="{{ route('spares.category.index') }}"><i class="fa fa-bars"></i> Категорії</a></li>
                    </ul>
                </li>
                <li class="nav-parent"><a href=""><i class="fa fa-suitcase"></i> <span>Акcесуари</span></a>
                    <ul class="children">
                        <li class="{{ strstr($currentRoute,'accessories.catalog')?'active':'' }}"><a href="{{ route('accessories.catalog.index') }}"><i class="fa fa-book"></i> Каталог</a></li>
                        <li class="{{ strstr($currentRoute,'accessories.brand')?'active':'' }}"><a href="{{ route('accessories.brand.index') }}"><i class="fa fa-heart"></i> Бренди</a></li>
                        <li class="{{ strstr($currentRoute,'accessories.category')?'active':'' }}"><a href="{{ route('accessories.category.index') }}"><i class="fa fa-bars"></i> Категорії</a></li>
                    </ul>
                </li>
                <li class="nav-parent"><a href=""><i class="fa fa-female"></i> <span>Одяг</span></a>
                    <ul class="children">
                        <li class="{{ strstr($currentRoute,'clothes.catalog')?'active':'' }}"><a href="{{ route('clothes.catalog.index') }}"><i class="fa fa-book"></i> Каталог</a></li>
                        <li class="{{ strstr($currentRoute,'clothes.brand')?'active':'' }}"><a href="{{ route('clothes.brand.index') }}"><i class="fa fa-heart"></i> Бренди</a></li>
                        <li class="{{ strstr($currentRoute,'clothes.category')?'active':'' }}"><a href="{{ route('clothes.category.index') }}"><i class="fa fa-bars"></i> Категорії</a></li>
                    </ul>
                </li>
            </ul>

        </div><!-- leftpanelinner -->
    </div><!-- leftpanel -->

    <div class="mainpanel">

        <div class="headerbar">

            <a class="menutoggle"><i class="fa fa-bars"></i></a>

            <div class="header-right">
                <ul class="headermenu">
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <img src="/backend/images/photos/loggeduser.png" alt=""/>
                                {{ Auth::user()->name }}
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                <li><a href="{{ route('profile') }}"><i class="glyphicon glyphicon-user"></i>
                                        Профіль</a></li>
                                <li><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                            class="glyphicon glyphicon-log-out"></i> Вихід</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div><!-- header-right -->

        </div><!-- headerbar -->

        <section>
            @yield('content')
        </section>

    </div><!-- mainpanel -->
</section>


<script src="{{ asset('backend/js/jquery-1.10.2.min.js') }}"></script>
<script src="{{ asset('backend/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/js/modernizr.min.js') }}"></script>
<script src="{{ asset('backend/js/retina.min.js') }}"></script>

<script src="{{ asset('backend/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('backend/js/toggles.min.js') }}"></script>
<script src="{{ asset('backend/js/jquery.cookies.js') }}"></script>

<script src="{{ asset('backend/js/jquery.datatables.min.js') }}"></script>
<script src="{{ asset('backend/js/chosen.jquery.min.js') }}"></script>

<script src="{{ asset('backend/js/custom.js') }}"></script>

@yield('scripts')
</body>
</html>
