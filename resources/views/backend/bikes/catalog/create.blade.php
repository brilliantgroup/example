@extends('layouts.admin')

@section('content')
    <div class="pageheader">
        <h2><i class="fa fa-heart"></i> Каталог <span>Головна</span><span>Велосипеди</span></h2>
        <div class="breadcrumb-wrapper">
            <div class="col-md-6 text-right"><a href="{{ route('bikes.catalog.index') }}"
                                                class="btn btn-primary btn-sm js-add">&laquo; Каталог</a></div>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Добавити товар</h4>
            </div>

            <form method="post" id="aform" action="{{ route('bikes.catalog.store') }}" class="form-horizontal"
                  enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="row">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Назва <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control"
                                   placeholder="Напишіть назву товару..."
                                   required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="category_id">Категорія <span
                                    class="asterisk">*</span></label>
                        <div class="col-sm-5">
                            <select class="form-control chosen-select" name="category_id" id="category_id"
                                    data-placeholder="Виберіть категорію..." required>
                                <option value=""></option>
                                @if ($categories->count())
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <label class="error" for="category_id"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="brand_id">Бренд <span
                                    class="asterisk">*</span></label>
                        <div class="col-sm-5">
                            <select class="form-control" id="brand_id" name="brand_id"
                                    data-placeholder="Виберіть бренд..." required>
                                <option value=""></option>
                                @if ($brands->count())
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                @endif


                            </select>
                            <label class="error" for="brand_id"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="year">Рік</label>
                        <div class="col-sm-8">
                            <input type="text" name="year" id="year" value="{{ old('year') ?? '2021' }}"
                                   class="form-control"
                                   placeholder="Напишіть рік моделі..."
                            />
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="year">Ціна ($ USD)</label>
                        <div class="col-sm-8">
                            <input type="text" name="price" id="price" value="{{ old('price') }}" class="form-control"
                                   placeholder=""
                            />
                        </div>
                    </div>
                    <hr>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="image">Картинка</label>
                        <div class="col-sm-8">
                            <input type="file" name="image" id="image" value="{{ old('image') }}"
                                   class="form-control"
                                   placeholder="Добавим картинку?)"
                            />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="short_description">Короткий опис <span
                                    class="asterisk">*</span></label>
                        <div class="col-sm-9">
                            <textarea name="short_description" id="short_description" rows="5" class="form-control"
                                      placeholder="Напишіть короткий опис..."
                                      required>Хороший, стильний, надійний, витривалий велосипед</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="description">Опис </label>
                        <div class="col-sm-9">
                            <textarea name="description" id="description" rows="5" class="form-control"
                                      placeholder="Напишіть опис..."
                            ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="features">Особливості </label>
                        <div class="col-sm-9">
                            <textarea name="features" id="features" rows="5" class="form-control"
                                      placeholder="Напишіть особливості..."
                            ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="components">Характеристики <br><small>(розмір,
                                колір і тд)</small> </label>
                        <div class="col-sm-9">
                            <textarea name="components" id="components" rows="25" class="form-control"
                                      placeholder="Напишіть характеристики..."
                                      require></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label-checkbox" for="newest">Новинка</label>
                        <div class="col-sm-9">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" id="newest" value="1"
                                       {{ old('newest')?'checked':'' }} name="newest"/>
                                <label for="newest"></label>
                            </div>
                            <label class="error" for="newest"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label-checkbox" for="special_offer">Акційний</label>
                        <div class="col-sm-9">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" id="special_offer" value="1"
                                       {{ old('special_offer')?'checked':'' }} name="special_offer"/>
                                <label for="special_offer"></label>
                            </div>
                            <label class="error" for="special_offer"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label-checkbox" for="sale">Розпродаж</label>
                        <div class="col-sm-9">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" id="sale" value="1"
                                       {{ old('active')?'checked':'' }} name="sale"/>
                                <label for="sale"></label>
                            </div>
                            <label class="error" for="sale"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label-checkbox" for="active">Активний запис</label>
                        <div class="col-sm-9">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" id="active" value="1"
                                       {{ old('active')?'checked':'' }} checked name="active"/>
                                <label for="active"></label>
                            </div>
                            <label class="error" for="active"></label>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary">{{ __('forms.save') }}</button>
                                <button type="reset" class="btn btn-default">{{ __('forms.reset') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>



@endsection

@section('scripts')
    <script src="/backend/js/jquery.validate.min.js"></script>
    <script>
        jQuery(document).ready(function () {

            $.validator.setDefaults({ignore: ":hidden:not(select)"})

            // Chosen Select
            jQuery("select").chosen({
                'min-width': '100px',
                'white-space': 'nowrap',
                disable_search_threshold: 10
            });


            // Basic Form
            jQuery("#aform").validate({
                rules: {
                    name: "required",
                    category_id: "required",
                    brand_id: "required",
                    short_description: "required",
                },
                messages: {
                    name: "Введіть будь ласка назву",
                    category_id: "Виберіть будь ласка категорію",
                    brand_id: "Виберіть будь ласка бренд",
                    short_description: "Введіть будь ласка короткий опис"
                },
                highlight: function (element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });
        });
    </script>
@endsection
