@php /** @var App\Models\Clothes $item */ @endphp
@extends('layouts.admin')

@section('content')
    <div class="pageheader">
        <h2><i class="fa fa-heart"></i> Каталог <span>Головна</span><span>Велосипеди</span></h2>
        <div class="breadcrumb-wrapper">
            <div class="text-right"><a href="{{ route('bikes.catalog.create') }}"
                                       class="btn btn-success btn-sm">Додати +</a>&nbsp;<a
                        href="{{ route('bikes.catalog.index') }}"
                        class="btn btn-primary btn-sm js-add">&laquo; Каталог</a></div>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Редагувати товар</h4>
            </div>
            <form id="aform" method="post" action="{{ route('bikes.catalog.update', ['item'=>$item]) }}"
                  class="form-horizontal" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="row">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Назва <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="name" value="{{ old('name')?old('name'):$item->name }}"
                                   class="form-control"
                                   placeholder="Напишіть назву товару..."
                                   required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Категорія <span class="asterisk">*</span></label>
                        <div class="col-sm-5">
                            <select class="form-control chosen-select" name="category_id" id="category_id"
                                    data-placeholder="Виберіть категорію..." required>
                                <option value=""></option>
                                @if ($categories->count())
                                    @foreach($categories as $category)
                                        <option
                                                value="{{ $category->id }}" {{ ($item->category_id==$category->id)?'selected':'' }}>{{ $category->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <label class="error" for="category_id"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="brand_id">Бренд <span
                                    class="asterisk">*</span></label>
                        <div class="col-sm-5">
                            <select class="form-control" id="brand_id" name="brand_id"
                                    data-placeholder="Виберіть бренд..." required>
                                <option value=""></option>
                                @if ($brands->count())
                                    @foreach($brands as $brand)
                                        <option
                                                value="{{ $brand->id }}" {{ ($item->brand_id==$brand->id)?'selected':'' }}>{{ $brand->name }}</option>
                                    @endforeach
                                @endif


                            </select>
                            <label class="error" for="brand_id"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Рік</label>
                        <div class="col-sm-8">
                            <input type="text" name="year" value="{{ old('year')?old('year'):$item->year }}"
                                   class="form-control"
                                   placeholder="Напишіть рік моделі..."
                            />
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="year">Ціна ($ USD)</label>
                        <div class="col-sm-8">
                            <input type="text" name="price" id="price" value="{{ old('price')?old('price'):$item->price }}" class="form-control"
                                   placeholder=""
                            />
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Картинка</label>
                        <div class="col-sm-8">
                            <div id="is-image"
                                 style="display: {{ (optional($item->media()->first())->id)?'block':'none' }}">
                                <img src="{{$item->getFirstMediaUrl('images', 'thumb')}}"/>
                                <a href="javascript:;" class="js-delete-img"
                                   data-id="{{ optional($item->media()->first())->id }}"><i
                                            class="fa fa-trash-o"></i> Видалити картинку</a>
                            </div>
                            <div id="not-image"
                                 style="display: {{ (optional($item->media()->first())->id)?'none':'block' }}">
                                <input type="file" name="image" id="image" value="{{ old('image') }}"
                                       class="form-control"
                                       placeholder="Добавим картинку?)"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="short_description">Короткий опис <span
                                    class="asterisk">*</span></label>
                        <div class="col-sm-9">
                            <textarea name="short_description" id="short_description" rows="5" class="form-control"
                                      placeholder="Напишіть короткий опис..."
                                      required>{{ old('short_description') ?? $item->short_description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="description">Опис <span
                                    class="asterisk">*</span></label>
                        <div class="col-sm-9">
                            <textarea name="description" id="description" rows="5" class="form-control"
                                      placeholder="Напишіть опис..."
                            >{{ old('description') ?? $item->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="features">Особливості </label>
                        <div class="col-sm-9">
                            <textarea name="features" id="features" rows="5" class="form-control"
                                      placeholder="Напишіть особливості..."
                            >{{ old('features') ?? $item->features }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="components">Характеристики <br><small>(розмір,
                                колір і тд)</small> </label>
                        <div class="col-sm-9">
                            <textarea name="components" id="components" rows="25" class="form-control"
                                      placeholder="Напишіть особливості..."
                                      require>{{ old('components') ?? $item->components }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label-checkbox">Новинка</label>
                        <div class="col-sm-9">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" id="newest" value="1"
                                       {{ old('newest')?'checked':(($item->newest)?'checked':'') }} name="newest"/>
                                <label for="newest"></label>
                            </div>
                            <label class="error" for="newest"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label-checkbox">Акційний</label>
                        <div class="col-sm-9">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" id="special_offer" value="1"
                                       {{ old('special_offer')?'checked':(($item->special_offer)?'checked':'') }} name="special_offer"/>
                                <label for="special_offer"></label>
                            </div>
                            <label class="error" for="special_offer"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label-checkbox">Розпродаж</label>
                        <div class="col-sm-9">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" id="sale" value="1"
                                       {{ old('sale')?'checked':(($item->sale)?'checked':'') }} name="sale"/>
                                <label for="sale"></label>
                            </div>
                            <label class="error" for="sale"></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label-checkbox">Активний запис</label>
                        <div class="col-sm-9">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" id="active" value="1"
                                       {{ old('active')?'checked':(($item->active)?'checked':'') }} name="active"/>
                                <label for="active"></label>
                            </div>
                            <label class="error" for="active"></label>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary">{{ __('forms.update') }}</button>
                                <button type="reset" class="btn btn-default">{{ __('forms.reset') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/backend/js/jquery.validate.min.js"></script>
    <script>
        jQuery(document).ready(function () {

            $.validator.setDefaults({ignore: ":hidden:not(select)"})

            // Chosen Select
            jQuery("select").chosen({
                'min-width': '100px',
                'white-space': 'nowrap',
                disable_search_threshold: 10
            });

            jQuery('.js-delete-img').on('click', function () {
                if (confirm("Ви впевнені?")) {
                    // Delete img
                    var id = ($(this).attr('data-id'));
                    var url = '/admin/delete/media/' + id;


                    $.ajax({
                        type: "GET",
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend: function () {
                            jQuery(".js-delete-img").html("Виконується...");
                        },
                        success: function (data) {
                            // js-delete-img
                            jQuery("#is-image").hide();
                            jQuery("#not-image").show();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
                return false;
            });

            // Basic Form
            jQuery("#aform").validate({
                rules: {
                    name: "required",
                    category_id: "required",
                    brand_id: "required",
                    short_description: "required",
                },
                messages: {
                    name: "Введіть будь ласка назву",
                    category_id: "Виберіть будь ласка категорію",
                    brand_id: "Виберіть будь ласка бренд",
                    short_description: "Введіть будь ласка короткий опис"
                },
                highlight: function (element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });
        });
    </script>
@endsection
