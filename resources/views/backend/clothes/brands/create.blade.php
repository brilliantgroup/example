@extends('layouts.admin')

@section('content')
    <div class="pageheader">
        <h2><i class="fa fa-heart"></i> Бренди <span>Головна</span><span>Одяг</span></h2>
        <div class="breadcrumb-wrapper">
            <div class="col-md-6 text-right"><a href="{{ route('clothes.brand.index') }}"
                                                class="btn btn-primary btn-sm js-add">&laquo; Бренди</a></div>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Добавити бренд</h4>
            </div>

            <form method="post" id="aform" action="{{ route('clothes.brand.store') }}" class="form-horizontal">
                @csrf
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="row">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Назва <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Напишіть назву бренду..."
                                   required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label-checkbox">Активний запис</label>
                        <div class="col-sm-9">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" id="active" value="1" {{ old('active')?'checked':'' }} name="active"/>
                                <label for="active"></label>
                            </div>
                            <label class="error" for="active"></label>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary">{{ __('forms.save') }}</button>
                                <button type="reset" class="btn btn-default">{{ __('forms.reset') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>



@endsection

@section('scripts')
    <script src="/backend/js/jquery.validate.min.js"></script>
    <script>
        jQuery(document).ready(function () {

            // Basic Form
            jQuery("#aform").validate({
                rules: {
                    name: "required",
                },
                messages: {
                    name: "Введіть будь ласка назву",
                },
                highlight: function (element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });
        });
    </script>
@endsection
