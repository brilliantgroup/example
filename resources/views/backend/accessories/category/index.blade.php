@extends('layouts.admin')

@section('content')
    <div class="pageheader">
        <h2><i class="fa fa-heart"></i> Категорії <span>Головна</span><span>Аксесуари</span></h2>
        <div class="breadcrumb-wrapper">
            <div class="col-md-6 text-right"><a href="{{ route('accessories.category.create') }}"
                                                class="btn btn-primary btn-sm">Додати +</a></div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>Назва</th>
                            <th style='width:100px'>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @include('backend.flash-message')

                        @if ($categories->count())
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->name }}</td>
                                    <td class="table-action">
                                        <a href="{{ route('accessories.category.edit', ['category'=>$category]) }}"><i
                                                class="fa fa-pencil"></i></a>
                                        <a href="javascript:;" class="delete-row" data-id="{{ $category->id }}"><i
                                                class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="/backend/js/lang/datatables.lang.json"></script>
    <script>
        jQuery('#table').dataTable({
            "sPaginationType": "full_numbers",
            "oLanguage": lang,
        });

        // Chosen Select
        jQuery("select").chosen({
            'min-width': '100px',
            'white-space': 'nowrap',
            disable_search_threshold: 10
        });

        // Delete row in a table
        jQuery('tbody').on('click', '.delete-row', function () {
            var c = confirm("Ви впевнені, шо хочете видалити?");
            if (c) {
                var id = ($(this).attr('data-id'));
                var url = '/admin/accessories/category/' + id;


                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: {"id": id, _method: 'delete'},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

                jQuery(this).closest('tr').fadeOut(function () {
                    jQuery(this).remove();
                });
            }

            return false;
        });
    </script>


@endsection
