@extends('layouts.admin')

@section('content')
    <div class="pageheader">
        <h2><i class="fa fa-heart"></i> Каталог <span>Головна</span><span>Аксесуари</span></h2>
        <div class="breadcrumb-wrapper">
            <div class="col-md-6 text-right"><a href="{{ route('accessories.catalog.create') }}"
                                                class="btn btn-primary btn-sm">Додати +</a></div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6 text-right">

                    </div>

                    <div class="col-md-6 text-right">Позначки:&nbsp;<small>
                            <span class="badge badge-success">&nbsp;</span> - новий <span class="badge badge-info">&nbsp;</span>
                            - розпродаж <span class="badge badge-warning">&nbsp;</span> - акційний
                        </small>
                    </div>
                </div>

            </div>
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th style='width:50px'>Фото</th>
                            <th style='width:30%'>Назва</th>
                            <th>Категорія</th>
                            <th>Н</th>
                            <th>Р</th>
                            <th>А</th>
                            <th style='width:100px'>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @include('backend.flash-message')
                        @if ($items->count())
                            @foreach($items as $item)

                                <tr>
                                    <td class="text-center"><img
                                            src="{{ $item->getFirstOrDefaultMediaUrl('images', 'thumb') }}"/></td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->categories->name }}</td>
                                    <td><span class="badge badge-{{ ($item->newest)?'success':'white' }}">&nbsp;</span>
                                    </td>
                                    <td><span
                                            class="badge badge-{{ ($item->special_offer)?'primary':'white' }}">&nbsp;</span>
                                    </td>
                                    <td><span class="badge badge-{{ ($item->sale)?'warning':'white' }}">&nbsp;</span>
                                    </td>
                                    <td class="table-action">
                                        <a href="{{ route('accessories.catalog.edit', ['item'=>$item]) }}"><i
                                                class="fa fa-pencil"></i></a>
                                        <a href="javascript:;" class="delete-row" data-id="{{ $item->id }}"><i
                                                class="fa fa-trash-o"></i></a>
                                    </td>

                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <style>
        .badge-white {
            background-color: transparent;
        }

        table > tbody > tr > td {
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('scripts')
    <script src="/backend/js/lang/datatables.lang.json"></script>
    <script>
        table = jQuery('#table').dataTable({
            "sPaginationType": "full_numbers",
            "oLanguage": lang,
            "iDisplayLength": 100
        });

        // Chosen Select
        jQuery("select").chosen({
            'min-width': '100px',
            'white-space': 'nowrap',
            disable_search_threshold: 10
        });

        // Delete row in a table
        jQuery('tbody').on('click', '.delete-row', function () {
            var c = confirm("Ви впевнені, шо хочете видалити?");
            if (c) {
                var id = ($(this).attr('data-id'));
                var url = '/admin/accessories/catalog/' + id;


                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: {"id": id, _method: 'delete'},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

                jQuery(this).closest('tr').fadeOut(function () {
                    jQuery(this).remove();
                });
            }

            return false;
        });
    </script>


@endsection
