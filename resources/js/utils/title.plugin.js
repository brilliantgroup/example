export default {
    install(Vue) {
        Vue.prototype.$title = function (titleKey) {
            const appName = 'Магазин велосипедної техніки Ровербайк'
            if (titleKey) {
                return `${titleKey.replace(/-/g, '|')} | ${appName}`
            } else {
                return `${appName}`
            }
        }
    }
}