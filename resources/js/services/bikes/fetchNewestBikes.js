import API from '../../api';

export default () => {

    const url = 'bikes/newest';

    const params = {

    };

    return API.get(url, {params})
        .then((response)=>Promise.resolve(response.data))
        .catch((error) => Promise.reject(error))
}
