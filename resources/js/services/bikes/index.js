import fetchAllBikes from "./fetchAllBikes";
import fetchNewestBikes from "./fetchNewestBikes";
import fetchBikeInfo from "./fetchBikeInfo";

export {
    fetchBikeInfo,
    fetchNewestBikes,
    fetchAllBikes
}
