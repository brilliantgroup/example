import axios from 'axios';

export default () => {

    const url = 'bikes/bike';

    const params = {

    };

    return axios.get(url, {params})
        .then((response)=>Promise.resolve(response.data))
        .catch((error) => Promise.reject(error))
}
