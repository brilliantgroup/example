/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
// Import Vue
import Vue from 'vue'
import Vuex from 'vuex'
import VueLodash from 'vue-lodash'
import lodash from 'lodash'

import VueMobileDetection from 'vue-mobile-detection'
import VueLazyload from "vue-lazyload"
import VueMeta from 'vue-meta'
import titlePlugin from './utils/title.plugin'
import Loader from './components/Loader'
import currencyFilter from "./filters/currency.filter";


const baseURL = '/api/v2'

import axios from 'axios';

// const baseURL = '/api/v1'
// Vue.prototype.$axios = axios.create({ baseURL })
Vue.use(VueMobileDetection);
Vue.use(Vuex);

Vue.use(VueLodash, { lodash: lodash })
Vue.use(VueLazyload);
Vue.use(titlePlugin);
Vue.use(VueMeta);

Vue.filter('currency', currencyFilter)

Vue.component('Loader', Loader)

import router from './router';
import App from './components/App';
import store from './store/store';
import "./custom.js";

const app = new Vue({
    el: '#app',
    store,
    router,               // <-- register router with Vue
    render: (h) => h(App) // <-- render App component
});
