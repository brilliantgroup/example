import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import modBikes from './bikes';
// import modAccessories from './accessories';


const store = new Vuex.Store({
    modules: {
        bikes: modBikes,
        // accessories
    },
    strict: false,
});

export default store;

