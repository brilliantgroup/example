import {
    fetchAllBikes,
    fetchNewestBikes,
    fetchBikeInfo
} from '../services/bikes/index';

const modBikes = {
    namespaced: true,

    state: {
        bikes: [],
        newestBikes: [],
        activeBike: {}
    },

    getters: {
        bikes: state => state.bikes,

        newestBikes: state => state.newestBikes,

        activeBike: state => state.activeBike
    },

    mutations: {
        setBikes(state, payload) {
            state.bikes = payload;
        },
        setNewestBikes(state, payload) {
            state.newestBikes = payload;
        },
        setActiveBike(state, payload) {
            state.activeBike = payload;
        },
    },

    actions: {
        requestBikes({ commit }, payload) {
            return new Promise((resolve, reject) => {
                fetchAllBikes(payload)
                    .then((response) => {
                        commit('setBikes', response.bikes);
                        resolve();
                    })
                    .catch((error) => reject(error))
            });
        },
        requestNewestBikes({ commit }, payload) {
            return new Promise((resolve, reject) => {
                fetchNewestBikes(payload)
                    .then((response) => {
                        commit('setNewestBikes', response);
                        resolve();
                    })
                    .catch((error) => reject(error))
            });
        },
        requestActiveBike({ commit }, payload) {
            return new Promise((resolve, reject) => {
                fetchBikeInfo(payload)
                    .then((response) => {
                        commit('setActiveBike', response.bike);
                        resolve();
                    })
                    .catch((error) => reject(error))
            });
        },
    }


}

export default modBikes;

