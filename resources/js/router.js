import Vue from "vue";
import VueRouter from "vue-router";

import PageHome from "./pages/home";
import PageAbout from "./pages/about";
import PageAccessories from "./pages/accessories";
import PageAccessoriesSale from "./pages/accessoriesSale";
import PageAccessory from "./pages/accessory";
import PageClothes from "./pages/clothes";
import PageClothesSale from "./pages/clothesSale";
import PageClothesPart from "./pages/clothespart";
import PageBike from "./pages/bike";
import PageBikes from "./pages/bikes";
import PageBikesSale from "./pages/bikesSale";
import PageSpares from "./pages/spares";
import PageSparesSale from "./pages/sparesSale";
import PageSparePart from "./pages/sparepart";
import PageNewest from "./pages/newest";
import PagePurchaseReturns from "./pages/purchaseReturns";
import PageServices from "./pages/services";
import PageWhereToBuy from "./pages/whereToBuy";
import PageWarranty from "./pages/warranty";
import PageNotFound from "./pages/404";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "home",
            component: PageHome
        },
        {
            path: "/about",
            name: "about",
            component: PageAbout
        },
        {
            path: "/accessories/:category?",
            name: "accessories",
            component: PageAccessories,
            props: true
        },
        {
            path: "/accessories-sale/:category?",
            name: "accessories-sale",
            component: PageAccessoriesSale,
            props: true
        },
        {
            path: "/accessory/:brand/:slug",
            name: "accessory",
            component: PageAccessory,
            props: true
        },
        {
            path: "/bikes/:category?",
            name: "bikes",
            component: PageBikes,
            props: true
        },
        {
            path: "/bikes-sale/:category?",
            name: "bikes-sale",
            component: PageBikesSale,
            props: true
        },
        {
            path: "/bike/:brand/:slug",
            name: "bike",
            component: PageBike,
            props: true
        },
        {
            path: "/clothes/:category?",
            name: "clothes",
            component: PageClothes,
            props: true
        },
        {
            path: "/clothes-sale/:category?",
            name: "clothes-sale",
            component: PageClothesSale,
            props: true
        },
        {
            path: "/clothes/:brand/:slug",
            name: "clothespart",
            component: PageClothesPart,
            props: true
        },
        {
            path: "/spares/:category?",
            name: "spares",
            component: PageSpares,
            props: true
        },
        {
            path: "/spares-sale/:category?",
            name: "spares-sale",
            component: PageSparesSale,
            props: true
        },
        {
            path: "/spares/:brand/:slug",
            name: "sparepart",
            component: PageSparePart,
            props: true
        },
        // {
        //     path: "/newest",
        //     name: "newest",
        //     component: PageNewest,
        // },
        // {
        //     path: "/services",
        //     name: "services",
        //     component: PageServices,
        // },
        {
            path: "/warranty",
            name: "warranty",
            component: PageWarranty,
        },
        {
            path: "/where-to-buy",
            name: "wheretobuy",
            component: PageWhereToBuy,
        },
        {
            path: "*",
            component: PageNotFound
        }
    ]
});

export default router;
