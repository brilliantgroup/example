<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'З цими данними ми нічого не знайшли.',
    'throttle' => 'Забагато спроб авторизуватись. Будь ласка спробуйте через :seconds секунд.',

    'login' => 'Вхід',
    'enter_login_and_password' => 'Введіть логін та пароль',
    'password' => 'Пароль',
    'confirm_password' => 'Повтор пароля',
    'reset_password' => 'Встановити новий пароль',

    'email_address' => 'E-Mail Address',
    'name' => 'Імя',
    'forgot_password' => 'Забули пароль?',
    'remind_password' => 'Нагадати пароль',

    'register' => 'Реєстрація',


];
