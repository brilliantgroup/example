<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ваш пароль був змінений успішно!',
    'sent' => 'Ми надіслали вам емейл з ссилкою для зміни паролю!',
    'throttled' => 'Будь ласка зачекайте.',
    'token' => 'Токен для змінення паролю невірний.',
    'user' => "Ми не можемо знайти користувача за цією емейл адресою.",

];
