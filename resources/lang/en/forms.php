<?php
return [
    'save' => 'Save',
    'update' => 'Update',
    'reset' => 'Reset',
    'message' => [
        'success' => 'Item saved successfully!',
        'error' => 'Something went wrong(!',
        'warning' => 'Warning!',
        'info' => 'You added new items, follow next step!',
    ]
];
