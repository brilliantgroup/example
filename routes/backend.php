<?php
Route::prefix('admin')->group(function () {


    Route::redirect('/', 'login');

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');

    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//    Route::post('register', 'Auth\RegisterController@register');

    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.update');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');

    Route::group(['middleware' => ['auth', 'web']], function () {

        Route::get('dashboard', 'Backend\DashboardController@index')->name('dashboard');
        Route::get('profile', 'Backend\ProfileController@index')->name('profile');

        // bikes brands
        Route::get('bikes/brands', 'Backend\BikesBrandController@index')->name('bikes.brand.index');
        Route::get('bikes/brands/create', 'Backend\BikesBrandController@create')->name('bikes.brand.create');
        Route::post('bikes/brands', 'Backend\BikesBrandController@store')->name('bikes.brand.store');
        Route::get('bikes/brands/{brand}/edit', 'Backend\BikesBrandController@edit')->name('bikes.brand.edit');
        Route::put('bikes/brands/{brand}', 'Backend\BikesBrandController@update')->name('bikes.brand.update');
        Route::delete('bikes/brand/{brand}', 'Backend\BikesBrandController@destroy')->name('bikes.brand.destroy');

        // bikes categories
        Route::get('bikes/categories', 'Backend\BikesCategoryController@index')->name('bikes.category.index');
        Route::get('bikes/categories/create', 'Backend\BikesCategoryController@create')->name('bikes.category.create');
        Route::post('bikes/categories', 'Backend\BikesCategoryController@store')->name('bikes.category.store');
        Route::get('bikes/categories/{category}/edit', 'Backend\BikesCategoryController@edit')->name('bikes.category.edit');
        Route::put('bikes/categories/{category}', 'Backend\BikesCategoryController@update')->name('bikes.category.update');
        Route::delete('bikes/category/{category}', 'Backend\BikesCategoryController@destroy')->name('bikes.category.destroy');

        // bikes catalog
        Route::get('bikes/catalog', 'Backend\BikesCatalogController@index')->name('bikes.catalog.index');
        Route::get('bikes/catalog/create', 'Backend\BikesCatalogController@create')->name('bikes.catalog.create');
        Route::post('bikes/catalog', 'Backend\BikesCatalogController@store')->name('bikes.catalog.store');
        Route::get('bikes/catalog/{item}/edit', 'Backend\BikesCatalogController@edit')->name('bikes.catalog.edit');
        Route::put('bikes/catalog/{item}', 'Backend\BikesCatalogController@update')->name('bikes.catalog.update');
        Route::delete('bikes/catalog/{item}', 'Backend\BikesCatalogController@destroy')->name('bikes.catalog.destroy');

        //spares
        Route::get('spares/brands', 'Backend\SparesBrandController@index')->name('spares.brand.index');
        Route::get('spares/brands/create', 'Backend\SparesBrandController@create')->name('spares.brand.create');
        Route::post('spares/brands', 'Backend\SparesBrandController@store')->name('spares.brand.store');
        Route::get('spares/brands/{brand}/edit', 'Backend\SparesBrandController@edit')->name('spares.brand.edit');
        Route::put('spares/brands/{brand}', 'Backend\SparesBrandController@update')->name('spares.brand.update');
        Route::delete('spares/brand/{brand}', 'Backend\SparesBrandController@destroy')->name('spares.brand.destroy');

        Route::get('spares/categories', 'Backend\SparesCategoryController@index')->name('spares.category.index');
        Route::get('spares/categories/create', 'Backend\SparesCategoryController@create')->name('spares.category.create');
        Route::post('spares/categories', 'Backend\SparesCategoryController@store')->name('spares.category.store');
        Route::get('spares/categories/{category}/edit', 'Backend\SparesCategoryController@edit')->name('spares.category.edit');
        Route::put('spares/categories/{category}', 'Backend\SparesCategoryController@update')->name('spares.category.update');
        Route::delete('spares/category/{category}', 'Backend\SparesCategoryController@destroy')->name('spares.category.destroy');

        Route::get('spares/catalog', 'Backend\SparesCatalogController@index')->name('spares.catalog.index');
        Route::get('spares/catalog/create', 'Backend\SparesCatalogController@create')->name('spares.catalog.create');
        Route::post('spares/catalog', 'Backend\SparesCatalogController@store')->name('spares.catalog.store');
        Route::get('spares/catalog/{item}/edit', 'Backend\SparesCatalogController@edit')->name('spares.catalog.edit');
        Route::put('spares/catalog/{item}', 'Backend\SparesCatalogController@update')->name('spares.catalog.update');
        Route::delete('spares/catalog/{item}', 'Backend\SparesCatalogController@destroy')->name('spares.catalog.destroy');

        //accessories
        Route::get('accessories/brands', 'Backend\AccessoriesBrandController@index')->name('accessories.brand.index');
        Route::get('accessories/brands/create', 'Backend\AccessoriesBrandController@create')->name('accessories.brand.create');
        Route::post('accessories/brands', 'Backend\AccessoriesBrandController@store')->name('accessories.brand.store');
        Route::get('accessories/brands/{brand}/edit', 'Backend\AccessoriesBrandController@edit')->name('accessories.brand.edit');
        Route::put('accessories/brands/{brand}', 'Backend\AccessoriesBrandController@update')->name('accessories.brand.update');
        Route::delete('accessories/brand/{brand}', 'Backend\AccessoriesBrandController@destroy')->name('accessories.brand.destroy');

        Route::get('accessories/categories', 'Backend\AccessoriesCategoryController@index')->name('accessories.category.index');
        Route::get('accessories/categories/create', 'Backend\AccessoriesCategoryController@create')->name('accessories.category.create');
        Route::post('accessories/categories', 'Backend\AccessoriesCategoryController@store')->name('accessories.category.store');
        Route::get('accessories/categories/{category}/edit', 'Backend\AccessoriesCategoryController@edit')->name('accessories.category.edit');
        Route::put('accessories/categories/{category}', 'Backend\AccessoriesCategoryController@update')->name('accessories.category.update');
        Route::delete('accessories/category/{category}', 'Backend\AccessoriesCategoryController@destroy')->name('accessories.category.destroy');

        Route::get('accessories/catalog', 'Backend\AccessoriesCatalogController@index')->name('accessories.catalog.index');
        Route::get('accessories/catalog/create', 'Backend\AccessoriesCatalogController@create')->name('accessories.catalog.create');
        Route::post('accessories/catalog', 'Backend\AccessoriesCatalogController@store')->name('accessories.catalog.store');
        Route::get('accessories/catalog/{item}/edit', 'Backend\AccessoriesCatalogController@edit')->name('accessories.catalog.edit');
        Route::put('accessories/catalog/{item}', 'Backend\AccessoriesCatalogController@update')->name('accessories.catalog.update');
        Route::delete('accessories/catalog/{item}', 'Backend\AccessoriesCatalogController@destroy')->name('accessories.catalog.destroy');

        //clothes
        Route::get('clothes/brands', 'Backend\ClothesBrandController@index')->name('clothes.brand.index');
        Route::get('clothes/brands/create', 'Backend\ClothesBrandController@create')->name('clothes.brand.create');
        Route::post('clothes/brands', 'Backend\ClothesBrandController@store')->name('clothes.brand.store');
        Route::get('clothes/brands/{brand}/edit', 'Backend\ClothesBrandController@edit')->name('clothes.brand.edit');
        Route::put('clothes/brands/{brand}', 'Backend\ClothesBrandController@update')->name('clothes.brand.update');
        Route::delete('clothes/brand/{brand}', 'Backend\ClothesBrandController@destroy')->name('clothes.brand.destroy');

        Route::get('clothes/categories', 'Backend\ClothesCategoryController@index')->name('clothes.category.index');
        Route::get('clothes/categories/create', 'Backend\ClothesCategoryController@create')->name('clothes.category.create');
        Route::post('clothes/categories', 'Backend\ClothesCategoryController@store')->name('clothes.category.store');
        Route::get('clothes/categories/{category}/edit', 'Backend\ClothesCategoryController@edit')->name('clothes.category.edit');
        Route::put('clothes/categories/{category}', 'Backend\ClothesCategoryController@update')->name('clothes.category.update');
        Route::delete('clothes/category/{category}', 'Backend\ClothesCategoryController@destroy')->name('clothes.category.destroy');

        Route::get('clothes/catalog', 'Backend\ClothesCatalogController@index')->name('clothes.catalog.index');
        Route::get('clothes/catalog/create', 'Backend\ClothesCatalogController@create')->name('clothes.catalog.create');
        Route::post('clothes/catalog', 'Backend\ClothesCatalogController@store')->name('clothes.catalog.store');
        Route::get('clothes/catalog/{item}/edit', 'Backend\ClothesCatalogController@edit')->name('clothes.catalog.edit');
        Route::put('clothes/catalog/{item}', 'Backend\ClothesCatalogController@update')->name('clothes.catalog.update');
        Route::delete('clothes/catalog/{item}', 'Backend\ClothesCatalogController@destroy')->name('clothes.catalog.destroy');

        Route::get('delete/media/{id}', 'Backend\DeleteMediaController');
    });


});
