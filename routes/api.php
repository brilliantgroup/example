<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {

//    bikes
    Route::get('/bikes/newest', 'BikesController@index');
    Route::get('/bikes/{category?}', 'BikesController@getItems');
    Route::get('/bike/{brand}/{slug}', 'BikesController@getItem');
    Route::get('/bikes-sale/{category?}', 'BikesController@getItemsSale');

//    spares
    Route::get('/spares/{category?}', 'SparesController@getItems');
    Route::get('/spares/{brand}/{slug}', 'SparesController@getItem');
    Route::get('/spares-sale/{category?}', 'SparesController@getItemsSale');

//    accessories
    Route::get('/accessories/{category?}', 'AccessoriesController@getItems');
    Route::get('/accessories-sale/{category?}', 'AccessoriesController@getItemsSale');
    Route::get('/accessory/{brand}/{slug}', 'AccessoriesController@getItem');

//    clothes
    Route::get('/clothes/{category?}', 'ClothesController@getItems');
    Route::get('/clothes-sale/{category?}', 'ClothesController@getItemsSale');
    Route::get('/clothes/{brand}/{slug}', 'ClothesController@getItem');


    Route::get('/menu', 'MenuController');
});
