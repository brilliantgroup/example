const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');


mix
    .setPublicPath(path.normalize('public'))
    .sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false
    })
    .js('resources/js/app.js', 'public/js').extract(['vue', 'vuex','axios','vue-axios','vue-router'])
    // .copy('resources/images/logo-bike.png', 'public/images')
    // .copy('resources/css/fa/css/font-awesome.min.css', 'public/css/fa/css/font-awesome.min.css')
    // .copy('resources/css/fa/fonts/fontawesome-webfont.eot', 'public/css/fa/fonts/fontawesome-webfont.eot')
    // .copy('resources/css/fa/fonts/fontawesome-webfont.svg', 'public/css/fa/fonts/fontawesome-webfont.svg')
    // .copy('resources/css/fa/fonts/fontawesome-webfont.ttf', 'public/css/fa/fonts/fontawesome-webfont.ttf')
    // .copy('resources/css/fa/fonts/fontawesome-webfont.woff', 'public/css/fa/fonts/fontawesome-webfont.woff')
    // .copy('resources/css/fa/fonts/fontawesome-webfont.woff2', 'public/css/fa/fonts/fontawesome-webfont.woff2')
